# Whakapapa mobile

## Development instructions

### Setup your computer

- **IMPORTANT** Make sure you have all the necessary tools such as Git, Node.js, npm, Android SDK, Android NDK etc.
  - [Follow the Cordova guide](https://cordova.apache.org/docs/en/9.x/guide/cli/index.html#installing-the-cordova-cli)
  - [Follow the Cordova Android platform guide](https://cordova.apache.org/docs/en/9.x/guide/platforms/android/index.html#requirements-and-support)
  - [Follow the nodejs-mobile-cordova guide](https://github.com/JaneaSystems/nodejs-mobile-cordova)
  - [Apply this Android NDK hack for "mipsel-linux-android"](https://github.com/JaneaSystems/nodejs-mobile-cordova#android)
- Run `npm install`
- Run `npm run setup`

### Setup your test device

- Make sure you have a **real Android device**, not an emulator
- Put your Android device in Developer Mode
  - Open the Android settings, scroll down to "About phone"
  - Scroll down to "Build number", and **tap it 7 times**
- Plug your Android device to your computer via USB
- On the Android device, **allow** your computer access to the device

### Build and run

- `npm run dev`, **if you want to run the full app on your computer**
  - Will take about 2 minutes to install the backend
  - Has Webpack's hot reloading enabled for the frontend
  - Will open your desktop's brower at `localhost:8080`
- `npm run dev-frontend`, **if you want to quickly run only the app's frontend on your computer**
  - Has Webpack's hot reloading enabled
  - Will open your desktop's brower at `localhost:8080`
- `npm run dev-backend`, **if you want to run only the app's backend on your computer**
  - Automatically restarts the server when you change code
  - You can experiment with the GraphQL server at `localhost:4000/graphql`
- `npm run android-dev`, **if you want to run the full app on the phone**
  - It will take about 5 minutes to do the following steps
  - Compile the Node.js (backend) project for mobile
  - Bundle the Vue.js frontend
  - Build the Android project
  - Run the app on your USB-connected device
- `npm run android-dev-update`, **if you want to run on the phone but only updated Vue.js code**
  - This assumes you have run `npm run android-dev` before
  - It will skip the compilation of the Node.js (backend) project

### npm scripts

* 🌤 `setup` - creates Cordova folders for the Android project
* 🌙 `teardown` - puts the repo in the state it was after git clone and npm install
* 🌺 `lint` - applies *prettier* to change code style of src files
* 💻 `dev` - runs frontend and backend servers in development mode and opens your local browser
* 💻 `dev-frontend` - runs the frondend server in development mode on your computer
* 💻 `dev-backend` - runs the backend server in development mode on your computer
* 📱 `android-dev` - compiles everything and runs the Android app on a device
* 📱 `android-dev-update` - compiles just the frontend and runs the Android app on a device

## Tech stack

From lowest layer in the stack to the highest:

- Android SDK and NDK
- Cordova
- Nodejs-mobile
- SSB
- Babel
- Webpack
- Vue.js
- Framework7

### Node.js (backend) project

The SSB server project can be found in `/src/nodejs-project`, this gets copied into `/www/nodejs-project` by the build scripts.

### Webpack

There is a Webpack bundler setup. It compiles and bundles all "front-end" resources. You should work only with files located in `/src` folder. Webpack config located in `scripts/webpack.config.js`.

Webpack has specific way of handling static assets (CSS files, images, audios). You can learn more about correct way of doing things on [official webpack documentation](https://webpack.js.org/guides/asset-management/).

### Assets

Assets (icons, splash screens) source images located in `assets-src` folder. To generate your own icons and splash screen images, you will need to replace all assets in this directory with your own images (pay attention to image size and format), and run the following command in the project directory:

```
framework7 generate-assets
```

Or launch UI where you will be able to change icons and splash screens:

```
framework7 generate-assets --ui
```

### Framework7 Documentation & Resources

* [Framework7 Core Documentation](https://framework7.io/docs/)
* [Framework7 Vue Documentation](https://framework7.io/vue/)

* [Framework7 Icons Reference](https://framework7.io/icons/)
* [Community Forum](https://forum.framework7.io)
