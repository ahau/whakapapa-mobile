import Vue from 'vue'
import Router from 'vue-router'

import Login from '@/views/Login.vue'
import PersonEdit from '@/views/PersonEdit'
import WhakapapaIndex from '@/views/WhakapapaIndex'
import WhakapapaShow from '@/views/WhakapapaShow'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  routes: [
    { path: '/', redirect: '/login' },
    { path: '/login', name: 'login', component: Login },
    // { path: '/logout', redirect: '/login' },

    { path: '/whakapapa', name: 'whakapapaIndex', component: WhakapapaIndex },
    { path: '/whakapapa/:id', name: 'whakapapaShow', component: WhakapapaShow },

    // { path: '/discovery', name: 'discovery', component: Discovery },

    // { path: '/person', name: 'personIndex', component: People },
    { path: '/person/:id/edit', name: 'personEdit', component: PersonEdit },
    // { path: '/person/:id', name: 'personShow', component: PersonShow },

    // { path: '/community/new', name: 'communityNew', component: CommunityNew },
    // { path: '/community/:id/edit', name: 'communityEdit', component: CommunityEdit },
    // { path: '/community/:id', name: 'communityShow', component: CommunityShow },

    { path: '*', redirect: '/' }
  ]
})
