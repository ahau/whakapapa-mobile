import Vue from 'vue'
import VuejsClipper from 'vuejs-clipper'
import { apolloProvider } from './apollo'
import vuetify from './vuetify'
import router from './router'
import nodejsClient from './nodejs-client.js'
import App from '@/App.vue'

if (process.env.PLATFORM === 'cordova') {
  nodejsClient.init()
}
Vue.use(VuejsClipper)

new Vue({
  apolloProvider,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
