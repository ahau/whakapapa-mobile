const noop = () => {}

const nodejsClient = {
  init({ startupListener, channelListener, onHTTPServerReady } = {}) {
    if (channelListener) nodejs.channel.setListener(channelListener)
    if (onHTTPServerReady) nodejs.channel.on('http-server', onHTTPServerReady)

    nodejs.start('main.js', startupListener || noop)
  }
}

export default nodejsClient
